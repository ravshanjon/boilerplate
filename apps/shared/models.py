from django.db.models import DateField, Model, SlugField
from django.utils import timezone


class TimeStampedModel(Model):
    created_at = DateField(default=timezone.now().date().isoformat())
    updated_at = DateField(auto_now=True)

    class Meta:
        abstract = True


class SlugStampedModel(TimeStampedModel):
    slug = SlugField(unique=True)

    class Meta:
        abstract = True
